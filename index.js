// [SECTION] While loop
/*
  - A while loop takes in an expression/condition
  - Expressions are any unit of code that can evaluated to a value 
  - if the condition evaluates to true, the statement inside the block will be executed.

*/

let count = 5;

while(count!==0) { //is not equal to zero  // count is not equal to zero 
	console.log("count:" + count);	
	count--;  // decrement count value count - 1;
	// it will stop when it hits false. 
}

// [SECTION] Do while

/*let number = Number(prompt("Give me a number:"));
// the "Number fuction" works similliar to the "parseInt" function

*/
// after executing once the while statement will evaluate wheater to run te next interation of the loop based on the given expression/condition (e.g. number less than 10)
// if the expression/contion is not true(false), the loo will stop


/*
What is parseInt?
Description. The parseInt function converts its first argument to a string, parses that string, then returns an integer or NaN . If not NaN , the return value will be the integer that is the first argument taken as a number in the specified radix
*/

/*do {
	console.log("Number: " + number);
	number += 1; //increment value into 1
} while(number < 10) //number must be less than 10 
*/

//while loop 
/*let n = 15;
while(number < 10){
	console.log("Number:" +number);
	number +=1;
}
*/

// [SECTION] for loop 
	// initialization // condition // change of value 
for(let count = 10; count <=20; count ++) {
	console.log(count);
}

let myString = "tupe";
//t = index 0
//u = index 1
//p = index 2
//e = index 3

console.log("myString length: " +myString.legnth);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);


// initialization 			//change of value insde a code
for(i=0; i < myString.length; i++){
	console.log(myString[i]);
}


let myName = "AlEx";
for(let i=0; i<myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
		console.log(3);
	}
	else {
		console.log(myName[i]);
	}
}